<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'wp-spaceline' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'mysql' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'mysql' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=~Wx?9! ]PZe;x}@!a#!{-T+c8Tz0v#S}!%r!,FFItfi$JwzxG$:Z|_@4*G$zL5h' );
define( 'SECURE_AUTH_KEY',  'dnqUr5zI_+p@VVT@l0=2#YY.J7`|ha?iL1! x~|#W#)!w5?Gv4LE-ll:$5rGs+C<' );
define( 'LOGGED_IN_KEY',    'Z5@#Y{v5LR:*n1YKu<Z}X5%r+D?G0(?a?hv*,79N;T77H@{!d=Z5.|>)z%kWsaR=' );
define( 'NONCE_KEY',        'OCrEiD}jjaZ!SfwM&8^K]l/)?5NMcp+~Y I^[PB[tw}X$rm&2@*B}1 EpZcE#jDN' );
define( 'AUTH_SALT',        'Vwa84Tw5Snn*l*6ZJY@v,w1riF$m/rlX^h=nbtFi_F`@y=Sx8p.657+m5%T;[>;1' );
define( 'SECURE_AUTH_SALT', '%:cPfeJhhUy(XO?ouYRe]eMR#n~Zl}BzyGsb,Pj0arZi.xnv>ro/<6M]D7VB:OAP' );
define( 'LOGGED_IN_SALT',   'P2=^&h5pY[J3)mV.Yk^6i`*$7@?!y_J4^2h2b9?j^2(I<wOhpFMRFRw-D%11q.b`' );
define( 'NONCE_SALT',       'e:+(habKMUl&udY+JcL*TM?n6k+e9U_kta4rsto[5DJ*q:c$`bTSCt%x<C`M!g_%' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
