<?php

/**
 * Добавляет страницу настройки темы в админку Вордпресса
 */
function mytheme_customize_register($wp_customize)
{
    // Секция Контакты
    // ID: themex_info
    $wp_customize->add_section(
        'themex_info',
        array(
            'title' => 'Информация',
            'priority' => 30,
            //'panel' => 'themex_panel_footer_customization',
        )
    );
    // Заголовок
    $wp_customize->add_setting(
        'themex_info__title',
        array(
            'default' => 'Информация',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__title_control',
        array(
            'label' => 'Заголовок для секции "Контакты"',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__title',
            'description' => 'пример: Наши контакты',
        )
    );
    // Город
    $wp_customize->add_setting(
        'themex_info__title_address',
        array(
            'default' => 'Москва',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__title_address_control',
        array(
            'label' => 'Подзаголовок для раздела "Адрес"',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__title_address',
            'description' => 'пример: Москва',
        )
    );
    // Адрес
    $wp_customize->add_setting(
        'themex_info__address',
        array(
            'default' => 'Пресненская наб., 12, Москва, 123317',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__address_control',
        array(
            'label' => 'Город',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__address',
            'description' => 'пример: Пресненская наб., 12, Москва, 123317',
        )
    );
    // Часы работы
    $wp_customize->add_setting(
        'themex_info__timework',
        array(
            'default' => 'ПН, ВТ, СР, ЧТ, ПТ, СБ: с 9:00 до 18:00, ВС: Выходной',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__timework_control',
        array(
            'label' => 'Время работы',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__timework',
            'description' => 'пример: ПН, ВТ, СР, ЧТ, ПТ, СБ: с 9:00 до 18:00, ВС: Выходной',
        )
    );
    // Контакты директора
    $wp_customize->add_setting(
        'themex_info__title_director',
        array(
            'default' => 'Контакты директора',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__title_director_control',
        array(
            'label' => 'Подзаголовок для раздела "Контакты директора"',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__title_director',
            'description' => 'пример: Контакты директора',
        )
    );
    // Телефон для связи
    $wp_customize->add_setting(
        'themex_info__tel',
        array(
            'default' => '+7 (123) 4567-78-90',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__tel_control',
        array(
            'label' => 'Телефон',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__tel',
            'description' => 'пример: +7 (123) 1234 56 78',
        )
    );
    // Почта для связи
    $wp_customize->add_setting(
        'themex_info__email',
        array(
            'default' => 'info@example.com',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__email_control',
        array(
            'label' => 'Почта',
            'type' => 'email',
            'section' => 'themex_info',
            'settings' => 'themex_info__email',
            'description' => 'пример: info@example.com',
        )
    );

    // Код карты
    $wp_customize->add_setting(
        'themex_info__code_map',
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__code_map_control',
        array(
            'label' => 'JS код карты',
            'type' => 'textarea',
            'section' => 'themex_info',
            'settings' => 'themex_info__code_map',
        )
    );
    // Дополнительный текст в внизу сайта
    $wp_customize->add_setting(
        'themex_info__text_footer',
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__text_footer_control',
        array(
            'label' => 'Дополнительный текст в внизу сайта',
            'type' => 'textarea',
            'section' => 'themex_info',
            'settings' => 'themex_info__text_footer',
        )
    );

    // Ссылка на аккаунт в Vk
    $wp_customize->add_setting(
        'themex_info__vk',
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__vk_control',
        array(
            'label' => 'Ссылка на аккаунт в Vk',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__vk',
            'description' => 'при пустом значении ссылка не странице не отобразится',
        )
    );
    // Ссылка на Instagram
    $wp_customize->add_setting(
        'themex_info__instagram',
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__instagram_control',
        array(
            'label' => 'Ссылка на Instagram аккаунт',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__instagram',
            'description' => 'при пустом значении ссылка не странице не отобразится',
        )
    );
    // Ссылка на аккаунт в Twitter
    $wp_customize->add_setting(
        'themex_info__twitter',
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__twitter_control',
        array(
            'label' => 'Ссылка на аккаунт в Twitter',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__twitter',
            'description' => 'при пустом значении ссылка не странице не отобразится',
        )
    );
    // Ссылка на аккаунт в Facebook
    $wp_customize->add_setting(
        'themex_info__facebook',
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__facebook_control',
        array(
            'label' => 'Ссылка на аккаунт в Facebook',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__facebook',
            'description' => 'при пустом значении ссылка не странице не отобразится',
        )
    );
    // Ссылка на аккаунт в Youtube
    $wp_customize->add_setting(
        'themex_info__youtube',
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
        'themex_info__youtube_control',
        array(
            'label' => 'Ссылка на аккаунт в Youtube',
            'type' => 'text',
            'section' => 'themex_info',
            'settings' => 'themex_info__youtube',
            'description' => 'при пустом значении ссылка не странице не отобразится',
        )
    );
}
add_action('customize_register', 'mytheme_customize_register');
