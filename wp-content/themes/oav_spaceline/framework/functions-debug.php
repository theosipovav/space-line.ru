<?php

define('FUNC_DEBUG', TRUE);

function d($value)
{
    if (!FUNC_DEBUG) return;
    echo '<div class="debug-d">';
    echo "<pre>" . print_r($value, true) . "</pre>";
    echo '</div>';
}

function dd($value)
{
    if (!FUNC_DEBUG) return;
    echo '<div style="background: #ffffff;padding: 1rem;font-size: 1rem;">';
    echo "<pre>" . print_r($value, true) . "</pre>";
    echo '</div>';
    exit();
}
function ddd($value)
{
    if (!FUNC_DEBUG) return;
    echo "<pre>" . htmlentities(print_r($value, true)) . "</pre>";
    exit();
}

function djs($value)
{
    if (!FUNC_DEBUG) return;
    printf('<script>console.log("%s");</script>', $value);
}



function get_include_contents($filename)
{
    if (is_file($filename)) {
        ob_start();
        include $filename;
        return ob_get_clean();
    }
    return false;
}

function the_breadcrumb()
{

    // получаем номер текущей страницы
    $pageNum = (get_query_var('paged')) ? get_query_var('paged') : 1;

    $separator = ' &raquo; '; //  »

    // если главная страница сайта
    if (is_front_page()) {

        if ($pageNum > 1) {
            echo '<a href="' . site_url() . '">Главная</a>' . $separator . $pageNum . '-я страница';
        } else {
            echo 'Вы находитесь на главной странице';
        }
    } else { // не главная

        echo '<a href="' . site_url() . '">Главная</a>' . $separator;


        if (is_single()) { // записи

            the_category(', ');
            echo $separator;
            the_title();
        } elseif (is_page()) { // страницы WordPress 

            the_title();
        } elseif (is_category()) {

            single_cat_title();
        } elseif (is_tag()) {

            single_tag_title();
        } elseif (is_day()) { // архивы (по дням)

            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $separator;
            echo '<a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . '</a>' . $separator;
            echo get_the_time('d');
        } elseif (is_month()) { // архивы (по месяцам)

            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $separator;
            echo get_the_time('F');
        } elseif (is_year()) { // архивы (по годам)

            echo get_the_time('Y');
        } elseif (is_author()) { // архивы по авторам

            global $author;
            $userdata = get_userdata($author);
            echo 'Опубликовал(а) ' . $userdata->display_name;
        } elseif (is_404()) { // если страницы не существует

            echo 'Ошибка 404';
        }

        if ($pageNum > 1) { // номер текущей страницы
            echo ' (' . $pageNum . '-я страница)';
        }
    }
}
