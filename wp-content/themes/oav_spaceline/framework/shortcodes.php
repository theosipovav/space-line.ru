<?php

/** 
 * [section_services]
 */
function shortcodeSectionServices($atts = [], $content = null)
{
    $out = '';
    global $post;
    $options = shortcode_atts(
        array(
            'title' => 'Заголовок',
            'subtitle' => 'Подзаголовок'
        ),
        $atts
    );
    $out .= '<div class="row">';
    $posts = get_posts(array(
        'numberposts' => 3,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'post',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => 'our-services',
            )
        ),
    ));
    foreach ($posts as $post) {
        setup_postdata($post);
        $out .= get_include_contents(locate_template('/templates/section-services--item.php'));
    }
    wp_reset_postdata();
    $out .= '</div>';
    return $out;
}
add_shortcode('section_services', 'shortcodeSectionServices');


/** 
 * [section_advantages col=3 max=6]
 * https://www.flaticon.com/packs/mintab-glyph-for-ios-3
 */
function shortcodeSectionAdvantages($atts = [], $content = null)
{
    $out = '';
    global $post;
    $options = shortcode_atts(
        array(
            'col' => 3,
            'max' => 6
        ),
        $atts
    );
    $out .= ' <div class="section-advantages">';
    $out .= '<div class="row">';
    $posts = get_posts(array(
        'numberposts' => 6,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'post',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => 'advantages',
            )
        ),
    ));
    foreach ($posts as $post) {
        setup_postdata($post);
        $out .= get_include_contents(locate_template('/templates/section-advantages--item.php'));
    }
    wp_reset_postdata();
    $out .= '</div>';
    $out .= '</div>';
    return $out;
}
add_shortcode('section_advantages', 'shortcodeSectionAdvantages');


/** 
 * [section_advantages col=3 max=6]
 * https://www.flaticon.com/packs/mintab-glyph-for-ios-3
 */
function shortcodeSectionNews($atts = [], $content = null)
{
    $out = '';
    global $post;
    $options = shortcode_atts(
        array(
            'col' => 3,
            'max' => 6
        ),
        $atts
    );
    $out .= '<div class="section-news">';
    $out .= '<div class="row">';
    $posts = get_posts(array(
        'numberposts' => 6,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'post',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => 'news',
            )
        ),
    ));
    foreach ($posts as $key => $post) {
        if ($key < 2) {
            setup_postdata($post);
            $out .= get_include_contents(locate_template('/templates/section-news--item-before.php'));
        }
    }
    $out .= '<div class="col-md-4">';
    $out .= '<div class="slidebar-news">';
    $out .= '<aside class="widget widget-recent-news">';
    $out .= '<ul class="recent-news">';
    $out .= '';
    foreach ($posts as $key => $post) {
        if ($key >= 2) {
            setup_postdata($post);
            $out .= get_include_contents(locate_template('/templates/section-news--item-after.php'));
        }
    }

    $out .= '</ul>';
    $out .= '</aside>';
    $out .= '</div>';
    $out .= '</div>';

    wp_reset_postdata();
    $out .= '</div>';
    $out .= '</div>';
    return $out;
}
add_shortcode('section_news', 'shortcodeSectionNews');











/** 
 * [section_services]
 */
function shortcodeSectionClients($atts = [], $content = null)
{
    $out = '';
    global $post;
    $options = shortcode_atts(
        [],
        $atts
    );
    $out .= '<div class="row">';
    $out .= '<div class="col-12">';
    $out .= '<div class="siema-client-buttons">';
    $out .= '<button id="siema-clients-prev" class="btn btn-primary btn-prev"><i class="fa fa-angle-left"></i></button>';
    $out .= '<button id="siema-clients-next" class="btn btn-primary btn-next"><i class="fa fa-angle-right"></i></button>';
    $out .= '</div>';
    $out .= '</div>';
    $out .= '</div>';


    $out .= '<div class="row">';
    $out .= '<div class="col">';
    $out .= '<div id="siema-clients" class="siema siema-clients mb-3">';
    $posts = get_posts(array(
        'numberposts' => 3,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'post',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => 'clients',
            )
        ),
    ));
    foreach ($posts as $post) {
        setup_postdata($post);
        $out .= get_include_contents(locate_template('/templates/section-clients--item.php'));
    }
    wp_reset_postdata();
    $out .= '</div>';
    $out .= '</div>';
    $out .= '</div>';
    wp_enqueue_script('section-client', THEME_ASSETS . "javascript/section-client.js", array('siema'));
    return $out;
}
add_shortcode('section_clients', 'shortcodeSectionClients');



/** 
 * Запрос ставки
 * [section_request_rate]
 * https://www.flaticon.com/packs/mintab-glyph-for-ios-3
 */
function shortcodeRequestRate($atts = [], $content = null)
{
    $out = '';
    global $post;
    $options = shortcode_atts(
        array(
            'col' => 3,
            'max' => 6
        ),
        $atts
    );
    $out .= ' <div class="section-advantages">';
    $out .= '<div class="row">';
    $posts = get_posts(array(
        'numberposts' => 6,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'post',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => 'advantages',
            )
        ),
    ));
    foreach ($posts as $post) {
        setup_postdata($post);
        $out .= get_include_contents(locate_template('/templates/section-advantages--item.php'));
    }
    wp_reset_postdata();
    $out .= '</div>';
    $out .= '</div>';
    return $out;
}
add_shortcode('section_request_rate', 'shortcodeRequestRate');













/** Отобразить стилизованный заголовок для секции на странице
 * Пример использования [title id="title_XXX"][/title]
 * Атрибуты:
 * @param array atts Массив атрибутов для шорткода
 * @param string content Содержание заголовка
 */
function shortcodeTitle($atts = [], $content = null)
{
    $out = '';
    $options = shortcode_atts(
        array(
            'id' => '',
        ),
        $atts
    );
    $out .= '<h2 id="' . $options["id"] . '" class="section--title display-4 text-middle">' . do_shortcode($content) . '</h2>';
    return $out;
}
add_shortcode('title', 'shortcodeTitle');

/** Отобразить стилизованный подзаголовок для секции на странице
 * Пример использования [subtitle id="subtitle_XXX"][/subtitle]
 * Атрибуты:
 * @param array atts Массив атрибутов для шорткода
 * @param string content Содержание подзаголовка
 */
function shortcodeSubtitle($atts = [], $content = null)
{
    $out = '';
    $options = shortcode_atts(
        array(
            'id' => '',
        ),
        $atts
    );
    $out .= '<p id="' . $options["id"] . '" class="mb-3">' . do_shortcode($content) . '</p>';
    return $out;
}
add_shortcode('subtitle', 'shortcodeSubtitle');
