<div class="col-md-4">
    <div class="iconbox-item">
        <div class="iconbox style1">
            <div class="box-header">
                <div class="icon-rounded-v2" style="background-image: url('<?= the_post_thumbnail_url() ?>');"></div>
                <div class="box-title">
                    <a href="'#" class="btn-link-noclick" title=""><?= get_the_title() ?></a>
                </div>
            </div>
            <div class="box-content">
                <?= get_the_content() ?>
            </div>
        </div>
    </div>
</div>