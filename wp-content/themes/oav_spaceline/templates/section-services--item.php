<div class="col-md-6 col-lg-4">
    <div class="imagebox-item">
        <div class="imagebox style1">
            <div class="imagebox-image"><img src="<?= the_post_thumbnail_url() ?>" alt="" /></div>
            <div class="imagebox-title">
                <h3>
                    <a href="#" title=""><?= get_the_title() ?><i class="fas fa-info-circle"></i></a>
                </h3>
            </div>
            <div class="imagebox-content">
                <div class="imagebox-desc"><?= get_the_content() ?></div>
                <div class="imagebox-button">
                    <a href="#" title="">Подробнее<i class="fas fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>