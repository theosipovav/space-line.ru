<div class="col-md-4">
    <article class="post style2">
        <div class="featured-post">
            <a href="<?= get_post_permalink() ?>" title="<?= get_the_title() ?>" class="post-image">
                <img src="<?= the_post_thumbnail_url() ?>" alt="<?= get_the_title() ?>" />
            </a>
            <ul class="post-date">
                <li class="day"><?= get_the_date('d'); ?></li>
                <li class="month"><?= get_the_date('M'); ?></li>
            </ul>
        </div>
        <div class="content-post">
            <h4 class="title-post">
                <a href="<?= get_post_permalink() ?>" title="<?= get_the_title() ?>"><?= get_the_title() ?></a>
            </h4>
            <div class="entry-post">
                <p><?= get_the_excerpt() ?></p>
            </div>
        </div>
    </article>
</div>