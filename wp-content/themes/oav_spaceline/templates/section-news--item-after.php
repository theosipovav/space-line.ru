<li>
    <div class="thumb">
        <span class="overlay-pop"></span>
        <a href="<?= get_post_permalink() ?>">
            <img src="<?= the_post_thumbnail_url() ?>" alt="<?= get_the_title() ?>" />
        </a>
    </div>
    <div class="text">
        <h4>
            <a href="<?= get_post_permalink() ?>" title="<?= get_the_title() ?>"><?= get_the_title() ?></a>
        </h4>
        <div class="entry-post">
            <p><?= get_the_date('d-M-yy'); ?></p>
        </div>
    </div>
</li>