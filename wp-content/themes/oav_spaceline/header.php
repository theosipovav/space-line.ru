<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php
    wp_body_open();
    ?>

    <body>
        <div class="boxed">
            <div class="preloader">
                <div class="clear-loading loading-effect-2">
                    <span></span>
                </div>
            </div>
            <header id="HeaderNav" class="header bg-color style3">
                <div class="container">
                    <div class="row">
                        <div class="header-wrap">
                            <div class="col-md-12">
                                <div class="nav-wrap d-flex">
                                    <div class="btn-menu">
                                        <span></span>
                                    </div>
                                    <nav id="mainnav" class="mainnav">
                                        <ul class="menu">
                                            <?php if (is_page()) { ?>
                                                <?php foreach (wp_get_nav_menu_items("Main", ['post_status ' => 'publish',]) as $key => $item) : ?>
                                                    <li><a href="<?= $item->url ?>" title="<?= $item->title ?>"><?= $item->title ?></a></li>
                                                <?php endforeach ?>
                                            <?php } else { ?>
                                                <?php foreach (wp_get_nav_menu_items("MainPost", ['post_status ' => 'publish',]) as $key => $item) : ?>
                                                    <li><a href="<?= $item->url ?>" title="<?= $item->title ?>"><?= $item->title ?></a></li>
                                                <?php endforeach ?>

                                            <?php } ?>
                                        </ul>
                                    </nav>
                                    <div class="main-contacts">
                                        <a href="tel:%20+7%20(8412)%2071-19-00" class="main-contacts-phone"><?= get_option('themex_info__tel') ?></a>
                                        <a href="mailto:%20director@re-montag.ru" class="main-contacts-email"><?= get_option('themex_info__email') ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>