jQuery(function ($) {
  //
  $(document).ready(function () {
    $("#HeaderNav").HeaderNav();

    $("#map-yandex").SiteMaps();
    $(".section--title").fitText(1.2, {
      minFontSize: "32",
      maxFontSize: "56px",
    });
    $(".section-header-card h1").fitText(1.2, {
      minFontSize: "32",
      maxFontSize: "81",
    });
    $(".section-header-card p").fitText(1.2, {
      minFontSize: "32",
      maxFontSize: "81",
    });
    $("header a").SoftAnchor({
      header: 70,
    });
    $(".footer-menu a").SoftAnchor({
      header: 70,
    });
    $("a.btn-get-consultation").SoftAnchor({
      header: 70,
    });
    $(".section-header--scroll").ScrollToStep({
      header: 70,
    });
  });
});
