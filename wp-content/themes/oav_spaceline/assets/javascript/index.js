(function ($) {
  "use strict";

  var isMobile = {
    Android: function () {
      return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
      return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
      return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
      return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
      return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows();
    },
  };

  var responsiveMenu = function () {
    var menuType = "desktop";

    $(window).on("load resize", function () {
      var currMenuType = "desktop";

      if (matchMedia("only screen and (max-width: 991px)").matches) {
        currMenuType = "mobile";
      }

      if (currMenuType !== menuType) {
        menuType = currMenuType;

        if (currMenuType === "mobile") {
          var $mobileMenu = $("#mainnav").attr("id", "mainnav-mobi").hide();
          var hasChildMenu = $("#mainnav-mobi").find("li:has(ul)");

          $("#header").after($mobileMenu);
          hasChildMenu.children("ul").hide();
          hasChildMenu.children("a").after('<span class="btn-submenu"></span>');
          $(".btn-menu").removeClass("active");
        } else {
          var $desktopMenu = $("#mainnav-mobi").attr("id", "mainnav").removeAttr("style");

          $desktopMenu.find(".submenu").removeAttr("style");
          $("#header").find(".nav-wrap").append($desktopMenu);
          $(".btn-submenu").remove();
        }
      }
    });

    $(".btn-menu").on("click", function () {
      $("#mainnav-mobi").slideToggle(300);
      $(this).toggleClass("active");
    });

    $(document).on("click", "#mainnav-mobi li .btn-submenu", function (e) {
      $(this).toggleClass("active").next("ul").slideToggle(300);
      e.stopImmediatePropagation();
    });
  };

  var goTop = function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 800) {
        $(".go-top").addClass("show");
      } else {
        $(".go-top").removeClass("show");
      }
    });

    $(".go-top").on("click", function () {
      $("html, body").animate({ scrollTop: 0 }, 1000, "easeInOutExpo");
      return false;
    });
  };

  var removePreloader = function () {
    $(window).load(function () {
      $(".preloader").css("opacity", 0);
      setTimeout(function () {
        $(".preloader").hide();
      }, 1000);
    });
  };

  $(function () {
    responsiveMenu();
    goTop();
    removePreloader();
  });
})(jQuery);

(function ($) {
  $.fn.HeaderNav = function () {
    var header = this;
    if ($(window).scrollTop() > 1) {
      $(header).addClass("fixed-header");
      fixed = true;
    } else {
      $(header).removeClass("fixed-header");
      fixed = false;
    }
    $(window).scroll(function () {
      if ($(this).scrollTop() > 1) {
        $(header).addClass("fixed-header");
        fixed = true;
      } else {
        $(header).removeClass("fixed-header");
        fixed = false;
      }
    });
  };

  $.fn.SiteMaps = function () {
    var maps = this;
    var visible = false;

    $(maps).click(function (e) {
      e.preventDefault();
      if (visible) {
        visible = false;
        $(maps).removeClass("maps-active");
      } else {
        visible = true;
        $(maps).addClass("maps-active");
      }
    });
  };

  $.fn.SoftAnchor = function (options) {
    var settings = $.extend(
      {
        header: 0,
      },
      options
    );
    var anchors = $(this);
    $(anchors).click(function () {
      elementClick = $(this).attr("href");
      destination = $(elementClick).offset().top - settings.header;
      $("html").animate({ scrollTop: destination }, 1000);
      return false;
    });
  };

  $.fn.ScrollToStep = function (options) {
    var settings = $.extend(
      {
        header: 0,
        step: $(window).height(),
      },
      options
    );
    var btn = $(this);
    $(btn).click(function (e) {
      e.preventDefault();
      $("html").animate({ scrollTop: settings.step - settings.header }, 1000);
    });
  };
})(jQuery);
