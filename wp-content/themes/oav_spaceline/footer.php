<!-- footer -->
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.2
 */

?>

<!-- Контакты -->
<section id="contacts" class="section-contacts">
    <div class="container">
        <div class="row">
            <div class="col-md">
                <h2 class="contacts--title display-3">
                    <?= get_option('themex_info__title') ?>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <h3 class="h3 mb-3"><?= get_option('themex_info__title_address') ?></h3>
                <ul class="contact-list">
                    <li>
                        <p class="icon"><i class="fas fa-map-marked"></i></p>
                        <p class="text"><?= get_option('themex_info__address') ?></p>
                    </li>
                    <li>
                        <p class="icon"><i class="ion-android-watch"></i></p>
                        <p class="text"><?= get_option('themex_info__timework') ?></p>
                    </li>
                </ul>
                <hr />
                <div class="h3 mb-3"><?= get_option('themex_info__title_director') ?></div>
                <ul class="contact-list">
                    <li>
                        <p class="icon"><i class="fas fa-phone"></i></p>
                        <p class="text"><?= get_option('themex_info__tel') ?></p>
                    </li>
                    <li>
                        <p class="icon"><i class="fas fa-envelope"></i></p>
                        <p class="text"><?= get_option('themex_info__email') ?></p>
                    </li>
                </ul>
            </div>
            <div class="col-lg-7">
                <script type="text/javascript" charset="utf-8" async src="<?= get_option('themex_info__code_map') ?>"></script>
            </div>
        </div>
    </div>
</section>
<footer id="footer" class="footer">
    <div class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget widget-information">
                        <ul class="information-footer">
                            <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#" title="<?= get_option('themex_contacts__email') ?>"><?= get_option('themex_info__email') ?></a></li>
                            <li><i class="fa fa-phone" aria-hidden="true"></i><a href="#" title="<?= get_option('themex_contacts__tel') ?>"><?= get_option('themex_info__tel') ?></a></li>
                            <li><i class="fa fa-map-marker" aria-hidden="true"></i><a href="#" title="<?= get_option('themex_contacts__address') ?>"><?= get_option('themex_info__address') ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row widget-box">
                <div class="col-md-4">
                    <div class="widget widget-text">
                        <p><?= get_option('themex_info__text_footer') ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="widget widget-dark widget-social" style="display: none;">
                        <h2 class="h2" style="text-align: center;">Мы в сетях</h2>
                        <ul>
                            <?php if (get_option('themex_contacts__vk') != '') : ?>
                                <li>
                                    <a class="widget-social--icon" href="#" title="">
                                        <img src="<?= THEME_ASSETS ?>images/icon/vk-white.svg" alt="" />
                                    </a>
                                </li>
                            <?php endif ?>
                            <?php if (get_option('themex_contacts__instagram') != '') : ?>
                                <li>
                                    <a class="widget-social--icon" href="#" title="">
                                        <img src="<?= THEME_ASSETS ?>images/icon/instagram-white.svg" alt="" />
                                    </a>
                                </li>
                            <?php endif ?>
                            <?php if (get_option('themex_contacts__twitter') != '') : ?>
                                <li>
                                    <a class="widget-social--icon" href="#" title="">
                                        <img src="<?= THEME_ASSETS ?>images/icon/twitter-white.svg" alt="" />
                                    </a>
                                </li>
                            <?php endif ?>
                            <?php if (get_option('themex_contacts__facebook') != '') : ?>
                                <li>
                                    <a class="widget-social--icon" href="#" title="">
                                        <img src="<?= THEME_ASSETS ?>images/icon/facebook-white.svg" alt="" />
                                    </a>
                                </li>
                            <?php endif ?>
                            <?php if (get_option('themex_contacts__youtube') != '') : ?>
                                <li>
                                    <a class="widget-social--icon" href="#" title="">
                                        <img src="<?= THEME_ASSETS ?>images/icon/youtube-white.svg" alt="" />
                                    </a>
                                </li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="widget widget-subscribe">
                        <form class="form form-dark m-auto" action="#" method="post">
                            <div class="form-group">
                                <label for="inputSubscribeEmail">Подпишитесь на новостную рассылку</label>
                                <input type="email" class="form-control" id="inputSubscribeEmail" aria-describedby="inputSubscribeEmail" placeholder="Электронная почту" />
                            </div>
                            <div class="form-group" style="text-align: center;">
                                <button type="submit" class="btn btn-primary">Подписаться</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-5" style="text-align: center;">
                    <p class="copyright">Copyright 2020 © <?= get_bloginfo("name") ?></p>
                </div>
                <div class="col-md-7">
                    <ul class="footer-menu m-auto">
                        <?php foreach (wp_get_nav_menu_items("Main", ['post_status ' => 'publish',]) as $key => $item) : ?>
                            <li><a href="<?= $item->url ?>" title="<?= $item->title ?>"><?= $item->title ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="button-go-top">
    <a href="#" title="" class="go-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>
</div>

<?php wp_footer(); ?>

</body>

</html>