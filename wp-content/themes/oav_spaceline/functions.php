<?php



//Error reporting
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_COMPILE_ERROR);

// Define constants
define('SITE_URL', home_url() . '/');
define('AJAX_URL', admin_url('admin-ajax.php'));
define('THEME_PATH', get_template_directory() . '/');
define('THEME_ASSETS', get_template_directory_uri() . '/assets/');
define('CHILD_PATH', get_stylesheet_directory() . '/');
define('THEME_URI', get_template_directory_uri() . '/');
define('CHILD_URI', get_stylesheet_directory_uri() . '/');
define('THEMEX_PATH', THEME_PATH . 'framework/');
define('THEMEX_URI', THEME_URI . 'framework/');
define('THEMEX_PREFIX', 'themex_');

//Include theme functions
include(THEMEX_PATH . 'functions-debug.php');
include(THEMEX_PATH . 'functions.php');
include(THEMEX_PATH . 'shortcodes.php');



// включим регистрацию реколл когда в настройках вордпресса она отключена
function dd3_open_rcl_register()
{
    $option = 1;
    return $option;
}
add_filter('rcl_users_can_register', 'dd3_open_rcl_register');


/* Отключаем админ панель для всех, кроме администраторов. */
if (!current_user_can('administrator')) :
    show_admin_bar(false);
endif;


/** Позволяет отключить "Панель инструментов" (Админ Бар).
 * Технически функция включает/отключает "Панель" для лицевой части (фронтэнда).
 * В админ-панели "Панель" отключить невозможно.
 * 
 * Отключить: add_filter('show_admin_bar', '__return_false');
 * Включить: add_filter('show_admin_bar', '__return_true');
 */
add_filter('show_admin_bar', '__return_false');






function customize_adverts_add($form)
{
    if ($form['name'] != "advert") {
        return $form;
    }
    foreach ($form["field"] as $key => $field) {
        if ($field["name"] == "advert_category") {
            $form["field"][$key]["is_required"] = true;
            $form["field"][$key]["validator"][] = array("name" => "is_required");
        }
    }
    return $form;
}
add_filter("adverts_form_load", "customize_adverts_add");
add_action("adverts_sh_manage_actions_after", "the_excerpt");







// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');





/**
 * Register and Enqueue Styles.
 */
function twentytwenty_register_styles()
{
    wp_enqueue_style('site-FontawesomeFreeFonts', THEME_ASSETS . 'fonts/FontawesomeFreeFonts.css');
    wp_enqueue_style('site-ionicons', THEME_ASSETS . 'fonts/ionIcons/ionicons.css');
    wp_enqueue_style('site-Roboto', THEME_ASSETS . 'fonts/Roboto/stylesheet.css');
    wp_enqueue_style('site-JetBrainsMono', THEME_ASSETS . 'fonts/JetBrainsMono/stylesheet.css');
    wp_enqueue_style('site-MontserratAlternates', THEME_ASSETS . 'fonts/MontserratAlternates.css');
    wp_enqueue_style('site-UniSans', THEME_ASSETS . 'fonts/UniSans/stylesheet.css');
    wp_enqueue_style('site-bootstrap', THEME_ASSETS . 'libs/bootstrap-4/css/bootstrap-custom.min.css');
    wp_enqueue_style('site-bxslider', THEME_ASSETS . 'css/jquery.bxslider.css');
    wp_enqueue_style('site-style', THEME_ASSETS . 'css/style.min.css');
}

add_action('wp_enqueue_scripts', 'twentytwenty_register_styles');



/**
 * Подключение скриптов для сайта
 */
function enqueueScriptSite()
{
    wp_enqueue_script('jq-easing', THEME_ASSETS . "javascript/jquery.easing.js", array('jquery'));
    wp_enqueue_script('jq-cookie', THEME_ASSETS . "javascript/jquery.cookie.js", array('jquery'));
    wp_enqueue_script('bootstrap', THEME_ASSETS . "javascript/bootstrap.min.js", array('jquery'));
    wp_enqueue_script('jq-fittext', THEME_ASSETS . "javascript/jquery.fittext.js", array('jquery'));
    wp_enqueue_script('siema', THEME_ASSETS . "javascript/siema.min.js", array('jquery'));
    wp_enqueue_script('index', THEME_ASSETS . "javascript/index.js", array('jquery'));
}
add_action('wp_enqueue_scripts', 'enqueueScriptSite');






/** Регистрирует поддержку новых возможностей темы
 * Выводит произвольное меню, созданное в панели: "внешний вид > меню" (Appearance > Menus).
 */
add_theme_support('menus');
/** Регистрирует поддержку новых возможностей темы
 * Позволяет устанавливать миниатюру посту
 */
add_theme_support('post-thumbnails');
add_theme_support('excerpt');
